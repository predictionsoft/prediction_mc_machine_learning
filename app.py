from flask import Flask, jsonify, send_file
from flask_pymongo import PyMongo
from dotenv import load_dotenv
import os
import pandas as pd
import numpy as np
import statsmodels.api as sm
from datetime import timedelta, datetime
import random
from copy import deepcopy
# Load environment variables from .env file
load_dotenv()

app = Flask(__name__)
app.config["MONGO_URI"] = os.getenv("MONGO_URI")
mongo = PyMongo(app)

@app.route('/api/v1/sales', methods=['GET'])
def get_sales():
    # Primero borramos todos los documentos en la colección 'predictions'
    mongo.db.predictions.delete_many({})

    # consultamos todos los documentos en la colección 'sales'
    sales = mongo.db.sales.find()
    data = pd.DataFrame(list(sales))
    if '_id' in data.columns:
        data = data.drop(columns=['_id'])
    # Convierte las columnas de fecha a formato datetime si no lo están
    data['dateBilling'] = pd.to_datetime(data['dateBilling'])

    # Agrupa por fecha y género, y calcula la suma de las ventas y el promedio de edad para cada grupo
    data = data.groupby(['dateBilling', 'gender']).agg({'amount': 'sum', 'age': 'mean'}).reset_index()

    prediction_dict = {}
    
    # Realiza las predicciones por género
    for gender in data['gender'].unique():
        # Filtra los datos para este género
        data_gender = data[data['gender'] == gender].copy()

        # Guarda las fechas originales en una variable
        original_dates = data_gender['dateBilling'].copy()

        # Convierte las fechas en números (días desde 1970-01-01)
        data_gender['dateBilling'] = (data_gender['dateBilling'] - pd.Timestamp("1970-01-01")) // pd.Timedelta('1D')

        # Procedemos a realizar la regresión lineal simple
        X = data_gender[['dateBilling']]
        Y = data_gender['amount']

        # Añade una constante a X
        X = sm.add_constant(X)

        model = sm.OLS(Y, X).fit()

        # Generamos un rango de fechas futuras
        last_date = original_dates.iloc[-1].date()
        next_year = last_date.year + 1
        # Crear un Timestamp para el 1 de enero del siguiente año
        start_date = pd.Timestamp(f"{next_year}-01-01")
        future_dates = [start_date + timedelta(days=i) for i in range(1, 365)]  # Predict for the next year

        # Convert future dates to the numeric format
        future_dates_numeric = [(date - pd.Timestamp("1970-01-01")) // pd.Timedelta('1D') for date in future_dates]

        # Create a DataFrame from future dates
        future_dates_df = pd.DataFrame(future_dates_numeric, columns=['dateBilling'])

        # Add a constant to future_dates_df
        future_dates_df = sm.add_constant(future_dates_df)

        # Predict for future dates
        future_predictions = model.predict(future_dates_df)

        # Add predictions to the dictionary
        for date, pred in zip(future_dates, future_predictions.tolist()):
            date_str = date.strftime("%Y-%m-%d")
            if date_str not in prediction_dict:
                prediction_dict[date_str] = {"date": date_str, "average_age": round(data_gender['age'].mean())}
            prediction_dict[date_str][f"prediction_{gender}"] = round(pred, 2)

    # Convert the dictionary to a list
    prediction_list = list(prediction_dict.values())
    prediction_list_copy = deepcopy(prediction_list)
    # Guardamos las predicciones en la colección 'predictions' de MongoDB
    mongo.db.predictions.insert_many(prediction_list)

    return jsonify({"sales_predictions": prediction_list_copy})


@app.route('/generate/<int:anio>/<int:mes>', methods=['GET'])
def generate_data(anio, mes):
    num_records = random.randint(5, 500)  # Generar entre 5 y 100 registros por día
    base = datetime(year=anio, month=mes, day=1)  # Fecha base para el mes y año proporcionados
    date_list = [base + timedelta(days=random.randint(0, 30)) for _ in range(num_records)]  # Sumamos días aleatorios dentro del mes
    dateBilling = pd.Series(date_list).dt.strftime('%Y-%m-%d').astype(str)
    
    start_year = 1960
    end_year = 1999
    birthdate = pd.Series(datetime(year=random.randint(start_year, end_year),
                                   month=random.randint(1, 12),
                                   day=random.randint(1, 28)) 
                          for _ in range(num_records)).dt.strftime('%Y-%m-%d').astype(str)
    
    gender = pd.Series(random.choice(['male', 'female']) for _ in range(num_records))
    amount = pd.Series(np.random.uniform(10, 500, num_records)).round(2)
    
    data = {
        'dateBilling': dateBilling.tolist(),
        'birthdate': birthdate.tolist(),
        'gender': gender.tolist(),
        'amount': amount.tolist()
    }

    df = pd.DataFrame(data)

    df['dateBilling'] = pd.to_datetime(df['dateBilling'])
    df = df.sort_values(by='dateBilling').reset_index(drop=True)
    df['dateBilling'] = df['dateBilling'].dt.strftime('%Y-%m-%d')  # Convertir fechas a formato string
    anio_str = str(anio)
    mes_str = str(mes)
    filename = anio_str+'_'+mes_str+'_ventas.xlsx'
    df.to_excel(filename, index=False)
    
    return send_file(filename, as_attachment=True)

if __name__ == "__main__":
    port = int(os.environ.get("FLASK_APP_PORT", 5000))
    debug = os.environ.get("FLASK_DEBUG", "False").lower() == "true"
    app.run(host='0.0.0.0', port=port, debug=debug)
